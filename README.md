# Avionos
## WBSO - Acties koppelen aan scrollen
We wilden parallax achtergronden en animaties die getriggerd zouden worden door te scrollen. Dit is gelukt met behulp van de ScrollMagic library.

Meer informatie:

- [ScrollMagic.js](https://janpaepke.github.io/ScrollMagic/)


## WBSO - FontLoader
We wilden een FOUC (flash of unstyled content) voorkomen in de header. Hiervoor moesten we detecteren wanneer de browser klaar was met het laden van de fonts. Dit is gelukt met behulp van de FontLoad library.

Meer informatie:

- [FontLoader](https://github.com/smnh/FontLoader)
- [Fighting the @font-face FOUT](http://www.paulirish.com/2009/fighting-the-font-face-fout/)