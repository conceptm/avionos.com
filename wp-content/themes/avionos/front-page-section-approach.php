<?php
/*
Template Name: Front Page Section Approach
*/
?>
<?php
    $post = get_post();
?>
<section id="<?php echo $post->post_name; ?>" class="front-page-section">
    <div class="background-container">
        <div class="background"></div>
    </div>
    <div class="container">
        <div class="main">
            <?php the_content(); ?>
        </div>
        <div class="bullets">
            <ul>
                <li><div class="content"><?php echo the_field('bullet_point_1'); ?></div></li>
                <li><div class="content"><?php echo the_field('bullet_point_2'); ?></div></li>
                <li><div class="content"><?php echo the_field('bullet_point_3'); ?></div></li>
            </ul>
        </div>
    </div>
</section>
