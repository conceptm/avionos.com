<?php
/*
Template Name: Leadership Person
*/
?>
<?php

$currentPageId = get_the_id();

$categories = array(
    247 => 'Partners',
    249 => 'Principals'
);

?>
<?php foreach ($categories as $categoryId => $categoryTitle): ?>
    <?php
        $query = new WP_Query(array(
            'post_type' => 'page',
            'post_parent' => $categoryId,
            'order' => 'ASC',
            'orderby' => 'menu_order'
        ));
    ?>
    <?php if ($query->have_posts()): ?>
        <div class="leadership-people">
            <h2><?php echo $categoryTitle; ?>:</h2>
            <ul>
                <?php while ($query->have_posts()): ?>
                    <?php $query->the_post(); ?>
                    <li<?php if (get_the_id() === $currentPageId): ?> class="active"<?php endif; ?>>
                        <a href="<?php the_permalink(); ?>">
                            <div class="avatar" style="background-image: url('<?php the_field('avatar'); ?>');"></div>
                            <div class="name"><?php the_title(); ?></div>
                            <div class="title"><?php the_field('title'); ?></div>
                        </a>
                    </li>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            </ul>
        </div>
    <?php endif; ?>
<?php endforeach; ?>
