<?php

get_header();

if (have_posts()) {
    while (have_posts()) {
        the_post();

        // Fetch all child pages
        $query = new WP_Query(array(
            'post_type' => 'page',
            'post_parent' => get_the_ID(),
            'order' => 'ASC',
            'orderby' => 'menu_order'
        ));

        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();
                $template = get_post_meta(get_the_ID(), '_wp_page_template', true);
                $template = str_replace('.php', '', $template);
                get_template_part($template);
            }

            wp_reset_postdata();
        }
    }
}

get_footer();
