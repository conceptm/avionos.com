<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<section class="page">
    <div class="container">
        <h1><?php the_title(); ?></h1>

        <div class="content-block">
           <?php the_content(); ?>
        </div>
    </div>
</section>

<?php endwhile; endif; ?>
<?php get_footer(); ?>
