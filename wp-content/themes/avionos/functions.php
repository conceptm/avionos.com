<?php

add_action( 'after_setup_theme', 'avionos_setup' );
function avionos_setup()
{
    require_once('lib/walker-nav-menu.php');
    require_once('lib/utils.php');

    load_theme_textdomain( 'avionos', get_template_directory() . '/languages' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'post-thumbnails' );
    global $content_width;
    if ( ! isset( $content_width ) ) $content_width = 640;
    register_nav_menus(
        array( 'main-menu' => __( 'Main Menu', 'avionos' ) )
    );
}

add_action( 'wp_enqueue_scripts', 'avionos_load_scripts' );
function avionos_load_scripts()
{
    $themeVersion = AvionosTheme::getVersion();

    wp_deregister_script('jquery');
    wp_register_script('jquery', get_template_directory_uri() . '/assets/javascripts/vendor/jquery-2.1.1.js', array(), $themeVersion);
    wp_register_script('tweenmax', get_template_directory_uri() . '/assets/javascripts/vendor/TweenMax.js', array(), $themeVersion);
    wp_enqueue_script('fontloader', get_template_directory_uri() . '/assets/javascripts/vendor/FontLoader.js', array(), $themeVersion);
    wp_enqueue_script('scrollto', get_template_directory_uri() . '/assets/javascripts/vendor/jquery.scrollTo.js', array('jquery'), $themeVersion);
    wp_enqueue_script('localscroll', get_template_directory_uri() . '/assets/javascripts/vendor/jquery.localScroll.js', array('jquery', 'scrollto'), $themeVersion);
    wp_enqueue_script('scrollmagic', get_template_directory_uri() . '/assets/javascripts/vendor/jquery.scrollmagic.js', array('jquery', 'tweenmax'), $themeVersion);
    wp_enqueue_script('scrollmagic.debug', get_template_directory_uri() . '/assets/javascripts/vendor/jquery.scrollmagic.debug.js', array('scrollmagic'), $themeVersion);
    wp_enqueue_script('magnific-popup', get_template_directory_uri() . '/assets/javascripts/vendor/jquery.magnific-popup.min.js', array(), $themeVersion);
    wp_enqueue_script('avionos', get_template_directory_uri() . '/assets/javascripts/avionos.js', array('localscroll'), $themeVersion);
}

add_action( 'comment_form_before', 'avionos_enqueue_comment_reply_script' );
function avionos_enqueue_comment_reply_script()
{
    if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}

add_filter( 'the_title', 'avionos_title' );
function avionos_title( $title ) {
    if ( $title == '' ) {
        return '&rarr;';
    } else {
        return $title;
    }
}

add_filter( 'wp_title', 'avionos_filter_wp_title' );
function avionos_filter_wp_title( $title )
{
    return $title . esc_attr( get_bloginfo( 'name' ) );
}

function avionos_custom_pings( $comment )
{
    $GLOBALS['comment'] = $comment;
    ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
    <?php
}

add_filter( 'get_comments_number', 'avionos_comments_number' );
function avionos_comments_number( $count )
{
    if ( !is_admin() ) {
        global $id;
        $comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
        return count( $comments_by_type['comment'] );
    } else {
        return $count;
    }
}

// Don't auto p.
remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );
add_filter( 'show_admin_bar', '__return_false', 9 );

function url_shortcode() {
    return get_bloginfo('url');
}
add_shortcode('url','url_shortcode');

add_filter( 'body_class', 'avionos_class_names' );
function avionos_class_names( $classes ) {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false) {
        $classes[] = 'disable-parallax';
    }

    return $classes;
}

add_filter('user_can_richedit', function($a) {
    return false;
}, 50);
