<?php
/*
Template Name: Front Page Section Intro
*/
?>
<?php
    $post = get_post();
?>
<section id="<?php echo $post->post_name; ?>" class="front-page-section">
    <div class="background"></div>
    <div class="container">
        <?php the_content(); ?>
    </div>
</section>
