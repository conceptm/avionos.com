<?php
/*
Template Name: Front Page Section Partners
*/
?>
<?php
    $post = get_post();
?>
<section id="<?php echo $post->post_name; ?>" class="front-page-section">
    <div class="container">
        <div class="main">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="bullets">
            <ul>
                <?php if( have_rows('partners') ){

                    while ( have_rows('partners') ) { the_row();

                    $image = get_sub_field('logo');
                ?>
                <li>
                    <?php if(isset($image['url'])){ ?>
                    <img src="<?php echo $image['url']; ?>" height="75">
                    <?php } ?>
                    <h2><?php the_sub_field('title'); ?></h2>
                    <p><?php the_sub_field('description'); ?></p>
                </li>

                <?php } } ?>
                <!-- <li>
                    <img src="<?php echo the_field('partner_1_logo'); ?>" height="75">
                    <h2><?php echo the_field('partner_1_title'); ?></h2>
                    <p><?php echo the_field('partner_1_description'); ?></p>
                </li>
                <li>
                    <img src="<?php echo the_field('partner_2_logo'); ?>" height="75">
                    <h2><?php echo the_field('partner_2_title'); ?></h2>
                    <p><?php echo the_field('partner_2_description'); ?></p>
                </li>
                <li>
                    <img src="<?php echo the_field('partner_3_logo'); ?>" height="75">
                    <h2><?php echo the_field('partner_3_title'); ?></h2>
                    <p><?php echo the_field('partner_3_description'); ?></p>
                </li>
 -->            </ul>
        </div>
    </div>
</section>
