<?php
/*
Template Name: Front Page Section Careers
*/
?>
<?php
    $post = get_post();
?>
<section id="<?php echo $post->post_name; ?>" class="front-page-section">
    <div class="container">
        <img class="graphic" width="260" height="260" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/assets/images/careers-graphic.png" alt="Careers">
        <div class="main">
            <?php the_content(); ?>
        </div>
        <div class="bullets">
            <?php echo the_field('values'); ?>
            <ul>
                <li><div class="content"><?php echo the_field('value_1'); ?></div></li>
                <li><div class="content"><?php echo the_field('value_2'); ?></div></li>
                <li><div class="content"><?php echo the_field('value_3'); ?></div></li>
            </ul>
        </div>
        <div class="reasons">
            <h3>Reasons to Work at Avionos</h3>
            <?php echo the_field('reasons'); ?>
        </div>
    </div>
</section>
