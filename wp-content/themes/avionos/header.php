<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <?php if (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false): ?>
        <meta name="viewport" content="width=1100" />
    <?php else: ?>
        <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <?php endif; ?>
    <title><?php wp_title( ' | ', true, 'right' ); ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
    <?php wp_head(); ?>
</head>
<body id="home" <?php body_class(); ?> data-spy="scroll" data-target="#header .menu">
    <div id="wrapper" class="hfeed">
        <header id="header" role="banner">
            <div class="container">
                <section id="branding">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>#home" title="<?php esc_attr_e( get_bloginfo( 'name' ), 'avionos' ); ?>" rel="home">
                        <img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/avionos-logo-white-cloud.svg" alt="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>">
                    </a>

                </section>
                <div class="mobile-buttons">
                    <a class="button" href="#address"><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/assets/images/phone-icon.png" width="13" height="19" alt="phone"></a>
                    <button name="menu">Menu</button>
                </div>
                <nav role="navigation" class="menu">
                    <?php
                        $menu = wp_nav_menu(array(
                            'theme_location' => 'main-menu',
                            'container' => false,
                            'menu_class' => 'nav',
                            'walker' => new Avionos_Walker_Nav_Menu
                        ));
                    ?>
                </nav>
            </div>
        </header>
