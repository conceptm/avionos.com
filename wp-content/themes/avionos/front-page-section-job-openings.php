<?php
/*
Template Name: Front Page Section Job openings
*/
?>
<?php
    $post = get_post();
?>
<section id="<?php echo $post->post_name; ?>" class="front-page-section">
    <div class="container">
        <div class="main">
            <?php the_content(); ?>
        </div>
    </div>
</section>
