<?php get_header(); ?>

<section class="page">
    <div class="container">
        <h1><?php _e( 'Not Found', 'avionos' ); ?></h1>

        <div class="content-block">
            <p><?php _e( 'Nothing found for the requested page.', 'avionos' ); ?></p>
        </div>
    </div>
</section>

<?php get_footer(); ?>
