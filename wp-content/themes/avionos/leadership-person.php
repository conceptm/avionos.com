<?php
/*
Template Name: Leadership Person
*/
?>
<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<section class="page">
    <div class="container">
        <h1>Leadership</h1>

        <div class="content-block">
            <div class="leadership-card">
                <div class="leadership-card-column-left">
                    <img src="<?php the_field('avatar'); ?>" width="240" height="240">
                    <?php if (!empty(get_field('linkedin_profile'))): ?>
                        <a href="<?php the_field('linkedin_profile'); ?>" target="_blank"><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/assets/images/icon-linkedin.png" width="30" height="29" alt="LinkedIn"></a>
                    <?php endif; ?>
                </div>
                <div class="leadership-card-column-right">
                    <h2><?php the_title() ?></h2>
                    <div class="title"><?php the_field('title'); ?></div>
                    <?php the_content(); ?>
                </div>
            </div>
        </div>

        <?php get_template_part('leadership-people') ?>
    </div>
</section>

<?php endwhile; endif; ?>
<?php get_footer(); ?>
