<?php
/*
Template Name: Front Page Section About
*/
?>
<?php
    $post = get_post();

    $image = get_field('video_image');
    if(isset($image['id'])){
        $thumb = wp_get_attachment_image_src( $image['id'], 'large' );
    }
?>
<section id="<?php echo $post->post_name; ?>" class="front-page-section">
    <div class="container">
        <div class="main">
            <?php the_content(); ?>
        </div>
        <?php if(isset($thumb)){ ?>
        <div class="video">
        <a href="<?php the_field('video_url'); ?>" target="_blank" class="popup" title="<?php the_field('video_title'); ?>" style="background-image: url('<?php echo $thumb[0]; ?>')">
            <div class="title">
                <h3><?php the_field('video_title'); ?></h3>
                <h4><?php the_field('video_subtitle'); ?></h4>
            </div>
        </a>
        </div>
        <?php } ?>
        <div class="bullets">
            <ul>
                <li><img class="graphic" width="66" height="66" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/assets/images/about-graphic-1.png">
                    <?php echo the_field('bullet_point_1'); ?></li>
                <li><img class="graphic" width="66" height="66" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/assets/images/about-graphic-2.png">
                    <?php echo the_field('bullet_point_2'); ?></li>
                <li><img class="graphic" width="66" height="66" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/assets/images/about-graphic-3.png">
                    <?php echo the_field('bullet_point_3'); ?></li>
            </ul>
        </div>
    </div>
</section>
