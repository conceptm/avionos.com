<?php
/*
Template Name: Front Page Section Contact
*/
?>
<?php
    $post = get_post();
?>
<section id="<?php echo $post->post_name; ?>" class="front-page-section">
    <div class="container">
    	<div class="address" id="address">
    		<?php echo the_field('address'); ?>

            <div class="contact-info">
                <?php echo the_field('contact_info'); ?>
            </div>
    	</div>

    	<div class="form">
    		<?php the_content(); ?>
    	</div>
    </div>
</section>
