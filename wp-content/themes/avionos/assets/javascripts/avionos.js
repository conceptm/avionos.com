$(function() {
  $.localScroll({
    duration: 200,
    offset: -60
  });

  /**
   * Font loading stuffs
   */
  var fontLoader = new FontLoader(['Lato'], {
    'fontsLoaded': function(error) {
      $('#intro .cloud').addClass('visible');
    }
  }, 3000);
  fontLoader.loadFonts();

  /**
   * Mobile menu
   */
  (function() {
    if ($(window).width() > 1060) {
      return;
    }

    var $menu = $('#header .menu');

    $('#header a[href="#contact"], #header a[href="#home"], #header .menu li a').click(function() {
      $menu.height(0);
    });

    $('#header button[name="menu"]').click(function() {
      if ($menu.height() === 0) {
        $menu.height($menu.find('li').length * 60);
      }
      else {
        $menu.height(0);
      }
    });
  })();

  $('.popup').on('click', function(e){
    $(e.currentTarget).blur();

    $.magnificPopup.open({
      items: {
        src: e.currentTarget.href
      },
      type: 'iframe'
    });
    return false;
  });

  // Responsive. Kind of.
  if ($(window).width() < 1060) {
    return;
  }

  var debug = false;
  var controller = new ScrollMagic();

  /**
   * Intro background parallax
   */
  (function() {
    if ($('body').hasClass('disable-parallax') || $('#intro').length < 1) {
      return;
    }

    var duration = $("#intro").height();
    var transform = "translateY(" + (duration*0.2) + "px)";

    var scene = new ScrollScene({ triggerHook: 'onLeave', triggerElement: '#intro', duration: duration })
      .setTween(TweenMax.to('#intro .background', 1, { transform: transform, force3D: true, ease: Linear.easeNone }))
      .addTo(controller);

    if (debug) {
      scene.addIndicators({ zindex: 100, suffix: "1" });
    }
  })();

  /**
   * Approach background parallax
   */
  (function() {
    if ($('body').hasClass('disable-parallax') || $('#approach').length < 1) {
      return;
    }

    var duration = $("#approach").height() + $(window).height();
    var transform = "translateY(" + (duration * 0.1) + "px)";
    $('#approach .background').css('transform', 'translate3d(0, ' + (duration * -0.1) +'px,0)');

    var scene = new ScrollScene({ triggerHook: 'onEnter', triggerElement: '#approach', duration: duration })
      .setTween(TweenMax.to('#approach .background', 1, { transform: transform, force3D: true, ease: Linear.easeNone }))
      .addTo(controller);

    if (debug) {
      scene.addIndicators({ zindex: 100, suffix: "2" });
    }
  })();

  /**
   * Header switch
   */
  (function() {
    var scene = new ScrollScene({ triggerHook: 'onLeave', triggerElement: '#wrapper', offset: 40 })
      .setClassToggle('#header', 'solid')
      .addTo(controller);

    if (debug) {
      scene.addIndicators({ zindex: 100, suffix: "header" });
    }
  })();

  /**
   * Nav bar highlighting
   */
  (function() {
    $('#header .menu li > a').each(function(index, element) {
      var $navItem = $(element);
      var href = $navItem.attr('href');
      var targetId = href.substr(href.indexOf('#'));
      var $target = $(targetId);

      if ($target.length < 1) {
        return;
      }

      var duration = $target.height();

      // Ugly monkey patch, but whatevs
      if ($target.attr('id') === 'job-openings') {
        var $careers = $('#careers');
        duration+= $careers.height();
        $target = $careers;
      }


      var scene = new ScrollScene({ triggerElement: $target, duration: duration })
        .setClassToggle($navItem.parent(), 'active')
        .addTo(controller);

      if (debug) {
        scene.addIndicators({ zindex: 100, suffix: "scrollspy" });
      }
    });
  })();

  /**
   * About animations
   */
  (function() {
    $('#about .bullets li').each(function(index, element) {
      var $element = $(element);
      var scene = new ScrollScene({ triggerHook: 'onEnter', triggerElement: $element, offset: 150 })
        .setClassToggle($element, 'visible')
        .addTo(controller);

      if (debug) {
        scene.addIndicators({ zindex: 100, suffix: "about" });
      }
    });

    $('#about .video').each(function(index, element) {
      var $element = $(element);
      var scene = new ScrollScene({ triggerHook: 'onEnter', triggerElement: $element, offset: 150 })
        .setClassToggle($element, 'visible')
        .addTo(controller);

      if (debug) {
        scene.addIndicators({ zindex: 100, suffix: "about" });
      }
    });
  })();

  /**
   * Approach animations
   */
  (function() {
    if ($('#approach').length < 1) {
      return;
    }

    var scene = new ScrollScene({ triggerHook: 'onEnter', triggerElement: '#approach', offset: 150 })
      .setClassToggle('#approach .container', 'visible')
      .addTo(controller);

    if (debug) {
      scene.addIndicators({ zindex: 100, suffix: "plane" });
    }

    $('#approach .bullets li').each(function(index, element) {
      var $element = $(element);
      var scene = new ScrollScene({ triggerHook: 'onEnter', triggerElement: $element, offset: 350 })
        .setClassToggle($element, 'visible')
        .addTo(controller);

      if (debug) {
        scene.addIndicators({ zindex: 200, suffix: "about" });
      }
    });
  })();

  /**
   * Careers animations
   */
  (function() {
    if ($('#careers').length < 1) {
      return;
    }

    var $graphic = $('#careers .graphic');
    var scene = new ScrollScene({ triggerHook: 'onEnter', triggerElement: '#careers', offset: 400})
          .setClassToggle($graphic, 'visible')
          .addTo(controller);

    if (debug) {
      scene.addIndicators({ zindex: 100, suffix: "graphic" });
    }

    $('#careers .bullets li').each(function(index, element) {
      var $element = $(element);
      var scene = new ScrollScene({ triggerHook: 'onEnter', triggerElement: $element, offset: 150 })
        .setClassToggle($element, 'visible')
        .addTo(controller);

      if (debug) {
        scene.addIndicators({ zindex: 100, suffix: "about" });
      }
    });

    $('#careers .reasons ul').each(function(index, element) {
      var $element = $(element);
      var scene = new ScrollScene({ triggerHook: 'onEnter', triggerElement: $element, offset: 150 })
        .setClassToggle($element, 'visible')
        .addTo(controller);

      if (debug) {
        scene.addIndicators({ zindex: 100, suffix: "about" });
      }
    });
  })();

  /**
   * Contact animations
   */
  (function() {
    if ($('#contact').length < 1) {
      return;
    }
    var scene = new ScrollScene({ triggerHook: 'onEnter', triggerElement: '#contact', offset: 550 })
      .setClassToggle('#contact .address', 'visible')
      .addTo(controller);

    if (debug) {
      scene.addIndicators({ zindex: 100, suffix: "contact" });
    }
  })();

  /**
   * Fixed contact button
   */
  (function() {
    if ($('#contact').length < 1) {
      return;
    }

    var scene = new ScrollScene({ triggerElement: '#contact' })
      .setClassToggle('#footer .contact-us-fixed', 'hidden')
      .addTo(controller);
  })();
});
