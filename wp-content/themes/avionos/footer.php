        <footer id="footer" role="contentinfo">
            <div id="copyright">
                <?php echo sprintf( __( '%1$s %2$s %3$s. All Rights Reserved.', 'avionos' ), '&copy;', date( 'Y' ), esc_html( get_bloginfo( 'name' ) ) ); ?>
            </div>
            <a class="contact-us-fixed" href="<?php echo esc_url( home_url( '/' ) ); ?>#contact" onclick="$.magnificPopup.close();">Contact us</a>
        </footer>
    </div>
<?php wp_footer(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51812849-2', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
