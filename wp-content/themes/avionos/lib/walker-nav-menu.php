<?php

class Avionos_Walker_Nav_Menu extends Walker_Nav_Menu {
    public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        $pageId = get_post_meta($item->ID, '_menu_item_object_id', true);
        $parent = get_post($pageId);
        $item->url = esc_url( home_url( '/' ) ) . '#' . $parent->post_name;

        return parent::start_el($output, $item, $depth, $args, $id);
    }
}
