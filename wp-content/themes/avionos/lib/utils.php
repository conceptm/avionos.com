<?php

class AvionosTheme {
    protected static $package;

    protected static function getPackage() {
        if (!self::$package) {
            self::$package = json_decode(file_get_contents(get_template_directory() . DIRECTORY_SEPARATOR . '/package.json'));
        }

        return self::$package;
    }

    public static function getVersion() {
        $package = self::getPackage();
        return $package->version;
    }
}
