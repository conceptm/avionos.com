<?php
/*
Template Name: Front Page Section Leadership
*/
?>
<?php
    $post = get_post();
?>
<section id="<?php echo $post->post_name; ?>" class="front-page-section">
    <div class="background-container">
        <div class="background"></div>
    </div>
    <div class="container">
        <div class="main">
            <h1><?php the_title(); ?></h1>
            <?php get_template_part('leadership-people') ?>
        </div>
    </div>
</section>
