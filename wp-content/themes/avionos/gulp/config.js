var src  = './assets',
    dest = './build';

module.exports = {
  sass: {
    src: src + '/stylesheets/**/*.scss',
    dest: '.'
  }
};
