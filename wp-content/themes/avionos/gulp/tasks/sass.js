var gulp         = require('gulp'),
    util         = require('gulp-util'),
    // sass         = require('gulp-ruby-sass'),
    sass         = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps   = require('gulp-sourcemaps'),
    config       = require('../config').sass;

gulp.task('sass', function() {
  return gulp.src(config.src)
    // .pipe(sass({
    //   sourcemap: 'inline',
    //   sourcemapPath: 'assets/sass',
    //   bundleExec: true
    // }))
    // .on('error', function(error) {
    //   util.log(error.message);
    // })
    // .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(sass({
      errLogToConsole: true
    }))
    // .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(autoprefixer({
        browsers: ["last 2 versions", "Firefox 17"]
    }))
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest(config.dest));
});
